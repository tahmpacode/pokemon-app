import mongoose from "mongoose";
import {battleSchema} from "../models/battleModel.js";

const Battle = mongoose.model("Battle", battleSchema);


// Returns all Battles
export function getAllBattle() {
    return Battle.find({}).sort({$natural: -1});
}

// Saves a battle
export function saveBattle(battle) { // async
    const newBattle = new Battle(battle);
    return newBattle.save();
}
