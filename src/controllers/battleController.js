import {fetchAllBattle, createBattle} from "../services/battleService.js";


// Dispatch action to get all previous battles.
export async function fetchAllBattleAction(request, response) {
    try {
        return await fetchAllBattle();
    } catch (e) {
        response.status(500).send('Battles couldn\' be fetched');
    }
}


// Dispatch action to save a battle and returns update elements for the GUI.
export async function createBattleAction(request, response) {
    const pokemonName1 = request.body.selectedPokemon[0];
    const pokemonName2 = request.body.selectedPokemon[1];

    try {
        const elementsForUpdate = await createBattle(pokemonName1, pokemonName2);
        response.send(elementsForUpdate);

    } catch (e) {
        response.status(500).send('Battle couldn\'t be saved');
    }
}
