import {fetchAllBattleAction} from "./battleController.js";
import {getPokemonInfos, getSortedPokemonList} from "../services/pokemonService.js";


// Returns the view index.pug. The view get a sorted pokemon list and all previous battles.
export async function getAllPokemonAction(request, response) {
    try {
        const sortedPokemonList = await getSortedPokemonList();
        const battles = await fetchAllBattleAction();

        return response.render('../views/index', { pokemons: sortedPokemonList, battles: battles });
    } catch (e) {
        response.status(500).send('An error happened 2');
    }
}

// Returns detail information for a specific pokemon. Image information and stats.
export async function getPokemonDetailAction(request, response) {
    try {
        const pokemonInfo = await getPokemonInfos(request.query.selectedPokemon);

        return response.render('../views/pokemonInfoCard', { pokemonInfo: pokemonInfo });
    } catch (e) {
        response.status(500).send('An error happened 3');
    }
}
