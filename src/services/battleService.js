import {getPokemonInfos} from "./pokemonService.js";
import {getAllBattle, saveBattle} from "../repositories/battleRepository.js";


// Returns all previous battles.
export function fetchAllBattle() {
    return getAllBattle();
}

// Saves a battle and returns the last battle.
export async function createBattle(pokemonName1, pokemonName2) {
    try {
        if (pokemonName1 && pokemonName2) {
            const battleResult = await estimateWinner(pokemonName1, pokemonName2);

            const battle = {
                pokemonId1: pokemonName1,
                pokemonId2: pokemonName2,
                battleResult: battleResult,
            }

            await saveBattle(battle);

            return generateTableEntryAndWinningInfo(battle)
        }
    } catch (e) {
        console.log(e);
    }
}

// Estimates the winner between two pokemons, depending on the sum of all stats.
async function estimateWinner(pokemon1, pokemon2) {
    try {
        const pokemonInfo1 = await getPokemonInfos(pokemon1);
        const pokemonInfo2 = await getPokemonInfos(pokemon2);

        return pokemonInfo1.stats.all > pokemonInfo2.stats.all ? pokemon1 : pokemonInfo1.stats.all < pokemonInfo2.stats.all ? pokemon2 : 'draw';
    } catch (e) {
        console.log(e);
    }
}

// Returns battle result and table update elements.
function generateTableEntryAndWinningInfo(battle) {
    // draw = 0, pokemon1 = 1, pokemon2 = 2
    const battleResult = battle.battleResult === 'draw' ? 'draw' : battle.battleResult === battle.pokemonId1 ? 'winner1' : 'winner2';

    let firstElement = ``;
    let secondElement = ``;
    let thirdElement = ``;
    let fourthElement = ``;

    // Prepare elements with colors for table and winning information
    if (battleResult === 'draw') {
        firstElement = `<tr><td style="background: palegoldenrod">${battle.pokemonId1}</td><td>:</td><td style="background: palegoldenrod">${battle.pokemonId2}</td></tr>`;
        secondElement = `<div id="winner1" hx-swap-oob="true"></div>`;
        thirdElement = `<div id="draw" hx-swap-oob="true" class="draw"><h4 class="result-text">DRAW</h4></div>`;
        fourthElement = `<div id="winner2" hx-swap-oob="true"></div>`;
    } else if (battleResult === 'winner1') {
        firstElement = `<tr><td style="background: lightgreen">${battle.pokemonId1}</td><td>:</td><td>${battle.pokemonId2}</td></tr>`;
        secondElement = `<div id="winner1" hx-swap-oob="true" class="winner"><h4 class="result-text">WINNER</h4></div>`;
        thirdElement = `<div id="draw" hx-swap-oob="true"></div>`;
        fourthElement = `<div id="winner2" hx-swap-oob="true"></div>`;
    } else if (battleResult === 'winner2') {
        firstElement = `<tr><td>${battle.pokemonId1}</td><td>:</td><td style="background: lightgreen">${battle.pokemonId2}</td></tr>`;
        secondElement = `<div id="winner1" hx-swap-oob="true"></div>`;
        thirdElement = `<div id="draw" hx-swap-oob="true"></div>`;
        fourthElement = `<div id="winner2" hx-swap-oob="true" class="winner"><h4 class="result-text">WINNER</h4></div>`;
    }

    return firstElement + secondElement + thirdElement + fourthElement;
}
