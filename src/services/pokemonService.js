import Pokedex from 'pokedex-promise-v2';

const PokemonDex = new Pokedex();


// Returns a sorted pokemon list.
export async function getSortedPokemonList() {
    try {
        const pokemons = await PokemonDex.getPokemonsList();
        return pokemons.results.sort((pokemonA, pokemonB) => {
            return pokemonA.name.localeCompare(pokemonB.name);
        });
    } catch (e) {
        console.log(e);
    }
}

// Returns all details about a specific pokemon.
export function getPokemonInfos(name) {
    return new Promise((resolve, reject) => {
        PokemonDex.getPokemonByName(name).then((responsePokeAPI) => {
            const pokemonInfo = {
                name: responsePokeAPI.name,
                weight: responsePokeAPI.weight,
                height: responsePokeAPI.height,
                order: responsePokeAPI.order,
                imgURL: responsePokeAPI.sprites.front_default,
                stats: {
                    hp: responsePokeAPI.stats[0].base_stat,
                    attack: responsePokeAPI.stats[1].base_stat,
                    defense: responsePokeAPI.stats[2].base_stat,
                    specialAttack: responsePokeAPI.stats[3].base_stat,
                    specialDefense: responsePokeAPI.stats[4].base_stat,
                    speed: responsePokeAPI.stats[5].base_stat,
                    all: responsePokeAPI.stats[0].base_stat + responsePokeAPI.stats[1].base_stat + responsePokeAPI.stats[2].base_stat + responsePokeAPI.stats[3].base_stat + responsePokeAPI.stats[4].base_stat + responsePokeAPI.stats[5].base_stat,
                }
            }
            resolve(pokemonInfo);
        })
    }).catch(e => {
        console.log(e);
    });
}



