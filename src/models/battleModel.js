import mongoose from 'mongoose';
import {url} from "../config/dbconfig.js";

const Schema = mongoose.Schema;


// Battle Schema
export const battleSchema = new Schema({
    pokemonId1: String,
    pokemonId2: String,
    battleResult: String, // 0 = 'draw', 1 = pokemon1, 2 = pokemon2
});

// Connects to the database
mongoose.connect(url).catch(e => console.log('No database connection established. Error message: ', e));

























// ----- Previous sqlite version, can be deleted -----

/*import sqlite from 'sqlite3';
import { Sequelize } from "sequelize";

const db = new sqlite.Database('./database/battle.db');
const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database/battle.db'
});

const Battles = sequelize.define('Battles',
    {
        pokemonId1: {
            type: Sequelize.STRING,
        },
        pokemonId2: {
            type: Sequelize.STRING,
        },
        winner: {
            type: Sequelize.STRING,
        },
    },
    {
        timestamps: true
    });


export function getAllBattle() {
    let query = 'SELECT * FROM Battles';

    return new Promise((resolve, reject) => {
        db.all(
            query,
            (error, results) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(results);
                }
            }
        );
    });
}

export function saveBattle(battle) {
    let query = 'INSERT INTO Battles (pokemonId1, pokemonId2, winner) VALUES (?, ?, ?)';

    return new Promise((resolve, reject) => {
        db.run(
            query,
            [battle.pokemonId1, battle.pokemonId2, battle.winner],
            (error, results) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(results);
                }
            }
        );
    });
}*/
