const {
    DB_USER = `${process.env.MONGODB_USER}`,
    DB_PASSWORD = `${process.env.MONGODB_PASSWORD}`,
    DB_HOST = `${process.env.MONGODB_DOCKER_PORT}`,
    DB_PORT = `${process.env.MONGODB_DOCKER_PORT}`,
    DB_NAME = `${process.env.MONGODB_DATABASE}`,
} = process.env;

export const url = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;
