import { Router } from 'express';
import { getAllPokemonAction, getPokemonDetailAction } from "../controllers/pokemonController.js";

const router = Router();


/**
 * @swagger
 * /pokemon:
 *  get:
 *     tags:
 *      - Pokemon
 *     description: Returns the view index.pug. The view get a sorted pokemon list and all previous battles.
 *     responses:
 *       200:
 *         description: All pokemons and all previous battles.
 */
router.get('/', getAllPokemonAction);


/**
 * @swagger
 * /pokemon/name:
 *  get:
 *     tags:
 *      - Pokemon
 *     description: Returns detail information for a specific pokemon. Image information and stats.
 *     responses:
 *       200:
 *         description: Detail information about a pokemon.
 */
router.get('/name', getPokemonDetailAction);

export { router };