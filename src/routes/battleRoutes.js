import { Router } from 'express';
import {fetchAllBattleAction, createBattleAction} from "../controllers/battleController.js";

const router = Router();


/**
 * @swagger
 * /battle:
 *  get:
 *     tags:
 *      - Battle
 *     description: Returns all previous battles.
 *     responses:
 *       200:
 *         description: All battles
 */
router.get('/', fetchAllBattleAction);


/**
 * @swagger
 * /battle:
 *  post:
 *     tags:
 *      - Battle
 *     description: Insert a battle and returns the information to update the GUI. In specific it returns winner information and the last battle for the table.
 *     responses:
 *       200:
 *         description: Saves a battle
 */
router.post('/', createBattleAction);

export { router };