FROM node:18.0.0

WORKDIR /pokemon-app
COPY package.json .
RUN npm install
COPY . .
CMD npm start