import swaggerJSDoc from 'swagger-jsdoc';

const swaggerDefinition = {
    info: {
        title: 'PokeAPI Database API',
        version: '1.0.0',
        description: 'API to access PokeAPI'
    },
    host: 'localhost: 8080',
    basePath: '/'
};

const options = {
    swaggerDefinition,
    apis: ['./src/routes/battleRoutes.js', './src/routes/pokemonRoutes.js']
};

export default swaggerJSDoc(options);