import {} from 'dotenv/config'
import express from 'express';
import morgan from "morgan";
import swaggerUI from "swagger-ui-express";
import swaggerSpec from './swagger.js';
import bodyParser from 'body-parser';
import { router as pokemonRouter } from './src/routes/pokemonRoutes.js';
import { router as battleRouter } from './src/routes/battleRoutes.js';

const app = express();
const PORT = process.env.NODE_DOCKER_PORT || 8080;

// Logger
app.use(morgan('common', { immediate: true }));

app.use(bodyParser.urlencoded({ extended: false }));

// To be able to use .pug files, this sets the view engine on pug.
app.set("view engine", "pug");


// Routes for pokemon
app.use('/pokemon', pokemonRouter);

// Routes for battle
app.use('/battle', battleRouter);

app.get('/', (request, response) => response.redirect('/pokemon'));

// Initialize swagger
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec));


app.listen(PORT, () => {
    console.log(`Server is running on port http://localhost:${PORT}.`);
});