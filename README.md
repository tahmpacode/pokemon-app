# Pokemon App

In dem Spiel können zwei Pokemons ausgewählt und ein Kampf zwischen ihnen gestartet werden. Das Pokemon mit dem 
höheren Wert (alle Stats zusammengezählt) gewinnt den Kampf. Sobald ein Gewinner fest steht, wird die Tabelle aktualisiert und eine 
zusätzliche Information des aktuellen Ergebnisses eingeblendet.  

## Getting started

Nachdem das Repository geklont wurde, musst du in das Hauptverzeichnis navigieren und den folgenden Befehl ausführen.

```
docker compose up --build 
```

Wenn alles korrekt gestartet wurde, kannst du die Anwendung auf folgender URL lokal starten. 

```
http://localhost:8080/
```
